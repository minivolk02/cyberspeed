---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "ita-ms.fullname" . }}
  labels:
{{ include "ita-ms.labels" . | indent 4 }}
  annotations:
    {{- range $key, $val := .Values.annotations }}
      {{ $key }}: {{ $val | quote }}
    {{- end }}
spec:
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "ita-ms.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
{{ include "ita-ms.replicaCount" . | indent 2 }}
  revisionHistoryLimit: {{ .Values.revisionHistoryLimit }}
  progressDeadlineSeconds: {{ .Values.progressDeadlineSeconds }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 1
  template:
    metadata:
      annotations:
        {{- range $key, $val := .Values.annotations }}
          {{ $key }}: {{ $val | quote }}
        {{- end }}
        {{- if .Values.forceRedeploy }}
          rollme: {{ randAlphaNum 5 | quote }}
        {{- end }}
      labels:
        app.kubernetes.io/name: {{ include "ita-ms.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
        image_tag: {{ (regexFind "[^:]+$" .Values.image ) | quote }}
    spec:
      serviceAccountName: {{ include "ita-ms.appName" . | lower }}
      securityContext:
        runAsUser: {{ if .Values.securityContext.runAsNonRoot }}{{ .Values.securityContext.runAsUser }}{{- else }}0{{- end }}
        runAsGroup: {{ if .Values.securityContext.runAsNonRoot }}{{ .Values.securityContext.runAsGroup }}{{- else }}0{{- end }}
        fsGroup: {{ if .Values.securityContext.runAsNonRoot }}{{ .Values.securityContext.fsGroup }}{{- else }}0{{- end }}
        {{- if .Values.securityContext.sysctls }}
        sysctls:
          {{- toYaml .Values.securityContext.sysctls | nindent 10 }}
        {{- end }}
      enableServiceLinks: {{ .Values.enableServiceLinks }}
      containers:
        - name: {{ include "ita-ms.fullname" . }}
          image: "{{ .Values.image }}"
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          securityContext:
            runAsNonRoot: {{ .Values.securityContext.runAsNonRoot }}
            readOnlyRootFilesystem: {{ .Values.securityContext.readOnlyRootFilesystem }}
          stdin: {{ .Values.stdin }}
          tty: {{ .Values.tty }}
          ports:
          {{- range .Values.ports }}
            - name: {{ .name }}
              containerPort: {{ .port }}
              protocol: {{ .protocol | default "TCP" }}
          {{- end }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          startupProbe:
            {{- toYaml .Values.startupProbe | nindent 12 }}
          readinessProbe:
            {{- toYaml .Values.readinessProbe | nindent 12 }}
          livenessProbe:
            {{- toYaml .Values.livenessProbe | nindent 12 }}
          env:
            - name: APP_NAME
              value: {{ include "ita-ms.appName" . }}
            - name: KUBERNETES_NODE_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
          {{- range $key, $val := .Values.env }}
            - name: {{ $key }}
              value: {{ $val | quote }}
          {{- end }}
          volumeMounts:
            {{- if .Values.persistentDumps }}
            - name: dumps
              mountPath: /dumps
            {{- end }}
          {{- if .Values.persistence.enabled }}
            - name: {{ include "ita-ms.appName" . }}-data
              mountPath: {{ .Values.persistence.mountPath }}
          {{- end }}
          {{- if .Values.volumeMounts }}
          {{- toYaml .Values.volumeMounts | nindent 12}}
          {{- end }}
      {{- if .Values.imagePullSecrets }}
      imagePullSecrets:
      {{- range $kv := .Values.imagePullSecrets }}
        - {{ toYaml $kv }}
      {{- end }}
      {{- end }}
      nodeSelector:
      {{- range $key, $val := .Values.nodeSelector }}
        {{ $key }}: {{ $val | quote }}
      {{- end }}
      hostNetwork: {{ .Values.hostNetwork }}
      affinity:
        # do not prefer to run additional copy of the app at the same host
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              topologyKey: kubernetes.io/hostname
              labelSelector:
                matchLabels:
                  app.kubernetes.io/name: {{ include "ita-ms.name" . }}
                  app.kubernetes.io/instance: {{ .Release.Name }}
      volumes:
        {{- if .Values.persistentDumps }}
        - name: dumps
          persistentVolumeClaim:
            claimName: {{ .Values.persistentDumpsPvcName }}
        {{- end }}
        {{- if .Values.persistence.enabled }}
        - name: {{ include "ita-ms.appName" . }}-data
          persistentVolumeClaim:
            claimName: {{ if .Values.persistence.existingClaim }}{{ .Values.persistence.existingClaim }}{{- else }}{{ include "ita-ms.appName" . }}-data{{- end }}
        {{- end }}
        {{- if .Values.volumes }}
        {{- toYaml .Values.volumes | nindent 8}}
        {{- end }}
