variable "project_name" {
  default = "possible-dream-417107"
}
variable "region" {
  default = "europe-west3"
}
provider "google" {
  credentials = file("project-creds.json")
  project     = var.project_name
  region      = var.region
}

terraform {
  backend "gcs" {
    bucket  = "my-terraform-state-bucket-cyberspeed-minivolk"
  }
}


resource "google_container_cluster" "cyberspeed_cluster" {
  name     = "cyberspeed-gke-cluster"
  location = "europe-west3"
  deletion_protection = false
  remove_default_node_pool = false

  node_locations = [
    "europe-west3-a"
  ]

  node_pool {
    name               = "default-pool"
    initial_node_count = 1
    node_config {
      machine_type = "e2-standard-2"
    }
    autoscaling {
      min_node_count = 1
      max_node_count = 2
    }
  }
}

provider "helm" {
  kubernetes {
    config_path            = "~/.kube/config"
    config_context         = "gke_${var.project_name}_${var.region}_${google_container_cluster.cyberspeed_cluster.name}"
  }
}

resource "helm_release" "nginx_ingress" {
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  name       = "nginx-ingress"
  version    = "4.10.0"
  namespace  = "default"

  values = [
    yamlencode({
      fullnameOverride = "ingress"
      controller = {
        replicaCount = 1
        service = {
          enabled = true
          type    = "LoadBalancer"
          annotations = {
            "cloud.google.com/load-balancer-type" = "Internal"
          }
        }
      }
    })
  ]
}

resource "helm_release" "argocd" {
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-cd"
  name       = "argo-cd"
  version    = "6.5.0"
  namespace  = "argocd"
  create_namespace = true

  values = [
    yamlencode({
      server = {
        ingress = {
          enabled = true
          hosts   = ["argocd.cyberspeed.test"]
          annotations = {
            "kubernetes.io/ingress.class" = "nginx"
          }
        }
      }
    })
  ]
}

