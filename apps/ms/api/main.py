from fastapi import FastAPI

app = FastAPI(
    title="CyperSpeed API",
)


@app.get('/')
def hello_world():
    return "<h1>Hello CyberSpeed</h1>"
